# Emulating a Raspberry Pi with QEMU
Goal: Emulate a [Raspberry Pi](https://www.raspberrypi.org/) with [QEMU](https://www.qemu.org/)
in order to run the [Raspbian O/S](https://www.raspberrypi.org/downloads/raspbian/) (based on Debian Linux).

The current setup is not ideal. For one thing, the maximum RAM allowed using the "versatile-pb" firmware is
256 Mb. In addition, only the most basic peripherals, a keyboard and mouse, are supported.

A number of articles have been written on this topic. Most are outdated, and the few recent ones are
missing key information.

## Software Required
1. QEMU system emulation binaries for ARM processors. On Ubuntu, qemu-system-arm.
2. [Raspbian Stretch with Desktop](https://downloads.raspberrypi.org/raspbian_latest), disk image.
3. Latest Debian Stretch kernel from the [qemu-rpi-kernel](https://github.com/dhruvvyas90/qemu-rpi-kernel) project.

## Procedure
1. Install qemu-system-arm (on Ubuntu, "sudo apt-get qemu-system-arm") to allow the emulation of devices
with arm processors like the Pi.
2. Create an emulation project directory, "~/Projects/rpitest" to hold the emulation files.
3. Clone the [qemu-rpi-kernel](https://github.com/dhruvvyas90/qemu-rpi-kernel) repo to another directory using git.
4. Copy the kernel image and matching versatile-pb.dtb file into the project directory (e.g. "rpitest").
Rename the kernel file to "kernel-qemu".
5. Download the latest Raspbian disk image and unzip into the project directory.
6. Find starting sector of the image's second partition using fdisk:

```bash
$ fdisk -l 2018-06-27-raspbian-stretch.img
Disk 2018-06-27-raspbian-stretch.img: 4.5 GiB, 4823449600 bytes, 9420800 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xbd98648d

Device                           Boot Start     End Sectors  Size Id Type
2018-06-27-raspbian-stretch.img1       8192   96663   88472 43.2M  c W95 FAT32 (LBA)
2018-06-27-raspbian-stretch.img2      98304 9420799 9322496  4.5G 83 Linux
```
8. Multiply that sector number by 512 and use the result as the offset in mounting the
image:

```bash
$ sudo mount 2018-06-27-raspbian-stretch.img -o offset=50331648 /mnt
```
9. Edit the ld.so.preload file to comment out its one and only line:

```
$ sudo vi /mnt/etc/ld.so.preload
```
This will enable devices like keyboards and mice to work in the emulator.

10. Unmount and rename the file to "rpitest.img".
```
$ sudo umount /mnt
$ mv 2018-06-27-raspbian-stretch.img rpitest.img
```
12. Convert the raw img file to qcow2 format:
```bash
$ qemu-img convert -f raw -O qcow2 rpitest.img rpitest.qcow2
```

## Starting the virtual machine
Copy the following code into a startup script called something like "rpistart.sh":

```bash
#!/usr/bin/env bash
$ sudo qemu-system-arm -kernel kernel-qemu \
-cpu arm1176 -m 256 \
-M versatilepb -dtb versatile-pb.dtb \
-no-reboot \
-serial stdio \
-append "root=/dev/sda2 panic=1 rootfstype=ext4 rw" \
-hda rpitest.qcow2 \
-net nic -net user \
-net tap,ifname=vnet0,script=no,downscript=no
```
Login using username "pi" and password "raspberry".

Your machine should be fully functional at this point, and able to reach the Internet over
the virtual network (vnet0) created along with it.

Do not update the system at this point, as the shipping image is 97% full.

## Expand the disk size
To update and do useful work with the system, the root partition (/dev/sda2) will need
to be expanded in size. The following steps will accomplish that.

1. Shut down the virtual machine and run qemu-img to increase the disk size:
```
$ qemu-img resize rpitest.qcow2 +4G
```

2. Start up the machine again and log in. Run fdisk to delete and then re-create
the second partition.
```
$ sudo fdisk /dev/sda
```
The commands to run within fdisk are:

```
print - to show the current disk layout
```
(note the starting sector of the second partition)

```
d - to delete a partition
2 - choose partition 2
n - create a new partition
p - make it primary
2 - partition number
xxxxx - the starting sector of the original partition 2
Enter - to accept the last sector of the disk as the end of the partition
w - to write all changes to disk
```

3. Complete the resizing operation by running resize2fs on the partition:
```bash
$ resize2fs /dev/sda2
```
4. Run df -h to confirm the expanded disk size is recognized by the system.

